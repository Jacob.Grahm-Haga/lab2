package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.*;

public class Pokemon implements IPokemon {
    //Field Variables
    String name;
    int HP;
    int maxHP;
    int str;
    Random random;

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return str;
    }

    @Override
    public int getCurrentHP() {
        return HP;
    }

    @Override
    public int getMaxHP() {
        return maxHP;
    }

    public boolean isAlive() {
        return (HP > 0);
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.str + this.str / 2 * random.nextGaussian());
        target.damage(damageInflicted);
        System.out.println(name + " attacks " + target.getName());
        System.out.println(target.getName() + " takes " + damageInflicted + " damage and is left with " + target.getCurrentHP() + "/" + target.getMaxHP() + "HP");
        if (target.getCurrentHP() <= 0) {
            System.out.println(target.getName() + " is defeated by " + name);
        }
    }

    @Override
    public void damage(int damageTaken) {
        HP -= Math.abs(damageTaken);
        if (HP <= 0) {
            HP = 0;
        }
    }

    @Override
    public String toString() {
        return (name + " HP: (" + HP + "/" + maxHP + ") STR: " + str);
    }

    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.HP = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHP = this.HP;
        this.str = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }
    
}