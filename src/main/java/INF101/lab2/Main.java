package INF101.lab2;

import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        //Initialize
        Main.pokemon1 = new Pokemon("Magnussaurus");
        Main.pokemon2 = new Pokemon("Vincentior");

        //Display pokemon
        System.out.println(pokemon1);
        System.out.println(pokemon2 + "\n");

        //Fight
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()) {
                pokemon2.attack(pokemon1);
            }
        }
    }
}
